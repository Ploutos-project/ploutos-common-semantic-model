#!/usr/bin/sh

# This is a script that Han used when his TopBraid trial expired and Protege wasn't working.

set -ex

sip1combined=$(mktemp /tmp/combined.XXXX.ttl)
sip5combined=$(mktemp /tmp/combined.XXXX.ttl)
sip9combined=$(mktemp /tmp/combined.XXXX.ttl)

cat ../ploutos.ttl ../sip1.ttl > $sip1combined
cat ../ploutos.ttl ../sip5.ttl > $sip5combined
cat ../ploutos.ttl ../sip9.ttl > $sip9combined

sparql --data=$sip1combined --file ./sip1-cq1.rq
sparql --data=$sip5combined --file ./sip5-cq1.rq
sparql --data=$sip9combined --file ./sip9-cq1.rq

rm "$sip1combined"
rm "$sip5combined"
rm "$sip9combined"
