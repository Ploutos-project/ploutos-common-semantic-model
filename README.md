# Ploutos Common Semantic Model

This repository contains the Ploutos Common Semantic Model (PCSM), a shared semantic model with common concepts and relations for the different pilots in the Ploutos project focussing on the agrifood supply chain and farm operations.

The model itself is in `ploutos.ttl`, which can be opened with an ontology editor.

In `sip1.ttl` through `sip9.ttl` are example data that showcase how the PCSM is used in the different sustainable innovation pilots (SIPs).

Other various files are from other authors and mostly used as imports for the PCSM.
